<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Music Player</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <!-- Styles -->
        <style>
           .main{
        position: absolute;
        background-color:#0abde3;
        width:680px;
        height: 500px;
        top:10%;
        left:25%;
        border:1px black solid;
    }
    hr{
        position: relative;
        top:13px;
        background-color: black;
        border: 1px black solid;
    }
    .fe3logo{
    position: relative;
    top:0px;
    left: 14px;
}
.uploadlogo{
    position: absolute;
    top:145px;
    left: 50px;
}
.allMlogo{
    position: absolute;
    top:190px;
    left:38px;
}
.Cplaylistlogo{
    position: absolute;
    top:235px;
    left: 38px;
}
    .main #minimize{
        position: absolute;
        left:90%;
        top: 5px;
    }
    .main #maximize{
        position:absolute;
        left: 93%;
        top: 5px;
    }
    .main #close{
        position: absolute;
        left: 96%;
        top:5px;
    }
    .main2{
        position:absolute;
        background-color: #5f27cd;
        width:523px;
        height: 375px;
        top: 31px;
        left:23%;
    }
    .searchBar{
    position: absolute;
    top: 20px;
    left: 40px;
    height: 33px;
    width: 240px;
    background:rgba(245, 245, 245, 0.1);
    border-style: solid;
    border-color: #222f3e;
    border-width: 1px;
    border-radius: 5px;
}
.searchBarLogo{
    position: absolute;
    top: 20px;
    left: 40px;
}
.BG{
    position: absolute;
    bottom: 0px;
    left: 60px;
}
.BG1{
    position: absolute;
    top: 20px;
    left: 290px;
}
.BG2{
    position: absolute;
    top: 0px;
    left: 335px;
}
.BG3{
    position: absolute;
    top:20px;
    left: 380px;
}
.main #backward{
    position: absolute;
    top: 440px;
    left: 350px;
}
.main #play{
    position: absolute;
    top: 440px;
    left: 400px;
}
.main #forward{
    position: absolute;
    top: 440px;
    left: 450px;
}
.song_duration{
    position: absolute;
    left:190px;
    bottom: 60px;
}
input[type="range"]{
    width: 450px;
    height: 3px;
}
.ttle{
    position: absolute;
    left: 95px;
    top: 165px;
    font-size:15px;
    color: white;
}
.artst{
    position: absolute;
    top: 165px;
    left: 223px;
    font-size:15px;
    color: white;
}
.albm{
    position: absolute;
    top: 165px;
    left: 343px;
    font-size:15px;
    color: white;
}
.dration{
    position: absolute;
    top: 165px;
    left: 445px;
    font-size:15px;
    color: white;
}
.table1{
    position: absolute;
    top:190px;
    left: 0px;
    height: 13%;
    width: 100%;
    background: #341f97;
    border-style: solid;
    border-color: #341f97;
    border-width: 1px;
    border-radius: 15px;
}
.some_t{
    position: absolute;
    left: 80px;
    top: 12px;
    font-size:13px;
    color: white;
}
.some_ar{
    position: absolute;
    top: 12px;
    left: 208px;
    font-size:13px;
    color: white;
}
.some_al{
    position: absolute;
    top: 12px;
    left: 328px;
    font-size:13px;
    color: white;
}
.dration_2{
    position: absolute;
    top: 12px;
    left: 465px;
    font-size:13px;
    color: white;
}
.table2{
    position: absolute;
    top:250px;
    left:0px;
    height: 13%;
    width: 100%;
    background: #341f97;
    border-style: solid;
    border-color: #341f97;
    border-width: 1px;
    border-radius: 15px;
}
.some_t1{
    position: absolute;
    left: 80px;
    top: 12px;
    font-size:13px;
    color: white;
}
.some_ar1{
    position: absolute;
    top:12px;
    left: 208px;
    font-size:13px;
    color: white;
}
.some_al1{
    position: absolute;
    top: 12px;
    left: 328px;
    font-size:13px;
    color: white;
}
.dration_3{
    position: absolute;
    top: 12px;
    left: 465px;
    font-size:13px;
    color: white;
}
.table3{
    position: absolute;
    top:310px;
    left: 0px;
    height: 13%;
    width: 100%;
    background: #341f97;
    border-style: solid;
    border-color: #341f97;
    border-width: 1px;
    border-radius: 15px;
}
.some_t2{
    position: absolute;
    left: 80px;
    top: 12px;
    font-size:13px;
    color: white;
}
.some_ar2{
    position: absolute;
    top:12px;
    left: 208px;
    font-size:13px;
    color: white;
}
.some_al2{
    position: absolute;
    top: 12px;
    left: 328px;
    font-size:13px;
    color: white;
}
.dration_4{
    position: absolute;
    top: 12px;
    left: 465px;
    font-size:13px;
    color: white;
}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="main">
            <p id="minimize"><i class="fa fa-window-minimize" aria-hidden="true"></i></p>
            <p id="maximize"><i class="fa fa-window-maximize" aria-hidden="true"></i></p>
            <p id="close"><i class="fa fa-window-close" aria-hidden="true"></i></p>
            <hr>
            <section class="fe3logo">
            <img style="width:125px; height:90px;" src="/img/F3-logo.gif">
            </section>
            <section class="uploadlogo">
            <img style="width:55px; height:40px;" src="/img/uplaod-music-icon.gif">
            </section>
            <b>
            <p style="font-size: 12px; color: black; margin-top: 55px; margin-left: 41px;">Upload Music</p>
            </b>
            <section class="allMlogo">
            <img style="width:77px; height:50px;" src="/img/all-music-icon.gif">
            </section>
            <b>
            <p style="font-size: 12px; color: black; margin-top: 30px; margin-left: 51px;">All Songs</p>
            </b>
            <section class="Cplaylistlogo">
            <img style="width:82px; height:62px;" src="/img/createplaylist-icon.gif">
            </section>
            <b>
            <p style="font-size: 12px; color: black; margin-top: 29px; margin-left: 38px;">Create Playlist</p>
            <p style="font-size: 12px; color: black; position:absolute; margin-top: 0px; margin-left: 20px;">Playlist 1</p>
            <p style="font-size: 12px; color: black; position:absolute; margin-top: 20px; margin-left: 20px;">Playlist 2</p>
            <p style="font-size: 12px; color: black; position:absolute; margin-top: 40px; margin-left: 20px;">Playlist 3</p>
            <p style="font-size: 12px; color: black; position:absolute; margin-top: 60px; margin-left: 20px;">Playlist 4</p>
            </b>
            <p id="backward"><i class="fa fa-step-backward" aria-hidden="true"></i></p>
            <p id="play"><i class="fa fa-play" aria-hidden="true"></i></p>
            <p id="forward"><i class="fa fa-step-forward" aria-hidden="true"></i></p>
            <div class="song_duration">
                <input type="range" min="0" max="100" value="0" id="duration_slider" onchange="change_duration()">
            </div>
            <div class="main2">
            <section class="BG">
                <img style="width:550px; height:360px;" src="/img/pers.gif">
            </section>
            <section class="BG1">
                <img style="width:70px; height:50px;" src="/img/blulogo.gif">
            </section>
            <section class="BG2">
                <img style="width:70px; height:50px;" src="/img/pnklogo.gif">
            </section>
            <section class="BG3">
                <img style="width:55px; height:35px;" src="/img/pnklogo2.gif">
            </section>
            <section class="searchBar">
            <b>
            <p style="font-size:14px; color:white; margin-left:41px; margin-top:4px;">Search</p>
            </b>
            </section>
            <section class="searchBarLogo">
            <img style ="height:37px; width:47;" src="/img/search-B.gif">
            </section>
            <b>
             <p style="font-size: 20px; color:white; margin-left:30px; margin-top:95px;">All Songs</p>
    
            
            <p class="ttle">Title</p>
            <p class="artst">Artist</p>                                          
            <p class="albm">Album</p>
            <p class="dration">Duration</p>
            </b>
    
            <div class="table1">
            <p class="some_t">Some title</p>
            <p class="some_ar">Some artist</p>                                          
            <p class="some_al">Some album</p>
            <p class="dration_2">3:45</p>
            </div>
    
            <div class="table2">
            <p class="some_t1">Some title</p>
            <p class="some_ar1">Some artist</p>                                          
            <p class="some_al1">Some album</p>
            <p class="dration_3">3:45</p>
            </div>
    
            <div class="table3">
            <p class="some_t2">Some title</p>
            <p class="some_ar2">Some artist</p>                                          
            <p class="some_al2">Some album</p>
            <p class="dration_4">3:45</p>
            </div>
    
            </div>
        </div>
    </body>
</html>
